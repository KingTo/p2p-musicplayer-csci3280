# P2P-MusicPlayer-CSCI3280
A music player that play .WAV music, which synchronised with lyrics.

# Description
- Main.java
> The main file
- Playmusic.java
> For play, pause, stop function
- Reader.java
> Read the raw data content of .WAV
- ReadLyric.java
> Read the Lyrics of the song
- Songdata.java
> for retrieve song data
- SqlQuery.java
> for connection to the SQL database, show the data detail in the music player
- Controller.java
> for the UI controlling, e.g. button action, key release, show lyrics

# Layout
![image](layout.PNG)
