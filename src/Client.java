
import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client extends Thread{
    private int port;
    private String host;
    private Socket client;
    Scanner consoleInput;
    
    public Client(String host, int port){
       this.port = port;
       this.host = host;
       consoleInput = new Scanner( System.in );
       //this.createSocket();
    }
    
    public void run() {
        String message = "";
        while(true){
            try{
                System.out.println("Just connected to " + client.getRemoteSocketAddress());
                while(!message.equals("end")){
                    System.out.println("==============Client==============");
                    System.out.println("Please Enter your Message...");
                    message = consoleInput.nextLine();
                    OutputStream outToServer = client.getOutputStream();
                    DataOutputStream out = new DataOutputStream(outToServer);
         
                    out.writeUTF(message+ "(from" + client.getLocalSocketAddress() + ")");
                    //out.writeUTF(message);
                    InputStream inFromServer = client.getInputStream();
                    DataInputStream in = new DataInputStream(inFromServer);
            
                    //System.out.println("==============Client==============");
                    System.out.println("Server says " + in.readUTF());
                }
                client.close();
            }catch (IOException e) {
                e.printStackTrace();
            break;
            }
         }
    
    }

   public void createSocket() {
      //String serverName = args[0];
      //int port = Integer.parseInt(args[1]);
      
      //Socket client;
      
        
        //message = consoleInput.nextLine();
        //int port = Integer.parseInt(port_str);
        try {    
         
         client = new Socket(host, port);
         System.out.println("Connecting to " + host + " on port " + port);
         this.start();
         
        } catch (IOException e) {
         e.printStackTrace();
        }
     }
}