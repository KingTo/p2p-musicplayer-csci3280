
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends Thread {
   private ServerSocket serverSocket;
   private int port;
   
   public Server(int port){
       this.port = port;
       //this.createSocket();
   }
   
   public void run() {
      String newMessage = "";
      while(true) {
         try {
            System.out.println("==============Server==============");
            System.out.println("Waiting for client on port " + 
            serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            System.out.println("Just connected to " + server.getRemoteSocketAddress());
            while(!newMessage.equals("end")) {
                DataInputStream in = new DataInputStream(server.getInputStream());
                newMessage = in.readUTF();
                System.out.println("==============Server==============");
                System.out.println(newMessage);
                DataOutputStream out = new DataOutputStream(server.getOutputStream());
                out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress()
                    + "\nGoodbye!");
            }
            server.close();
         } catch (SocketTimeoutException s) {
            System.out.println("Socket timed out!");
            break;
         } catch (IOException e) {
            e.printStackTrace();
            break;
         }
      }
   }
   
   public void createSocket() {
       try {
           serverSocket = new ServerSocket(port);
           this.start();
       } catch (IOException e) {
          e.printStackTrace();
       }
   }
}
