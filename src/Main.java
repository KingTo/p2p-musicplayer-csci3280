import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import readSonginfo.ReadLyric;

public class Main extends Application {
    private static Server[] server_threads;
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("My Music Player");
        primaryStage.setScene(new Scene(root, 900, 600));
        primaryStage.show();
        
        server_threads = new Server[3];
        Server server1 = new Server(6066);
        server1.createSocket();
        server_threads[0] = server1;
        
        Server server2 = new Server(6067);
        server2.createSocket();
        server_threads[1] = server2;
        
        Server server3 = new Server(6068);
        server3.createSocket();
        server_threads[2] = server3;
    }


    public static void main(String[] args) {
         launch(args);
    }
}
