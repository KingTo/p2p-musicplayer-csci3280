
package readSonginfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;


public class ReadLyric {
    private String LyricPath;
    public ReadLyric(String path){
    LyricPath=path;
    }
   public String[] Readfile(){
       String[] LyricInfo=new String[100];
       int i=0;
        try {
            Scanner input = new Scanner(LyricPath);
            File file = new File(input.nextLine());
            input = new Scanner(file);
            while (input.hasNextLine()) {
                
                String line = input.nextLine();
                LyricInfo[i] = line;
                if(i==0){
                LyricInfo[0]=line.substring(1,line.length());
                }
                i++;
                
            }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return LyricInfo;
   }
    
}
