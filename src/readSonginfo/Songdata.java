
package readSonginfo;


public class Songdata {
 
    private String Name;
    private String Time;
    private String Artist;
    private String Album;
    public Songdata(String name,String time,String artist,String album){
    this.Name=name;
    this.Time=time;
    this.Artist=artist;
    this.Album=album;
    }
    public void setName(String Name){
        this.Name=Name;
    }   
    public void setTime(String Time){
        this.Time=Time;
    }  
    public void setArtist(String Artist){
        this.Artist=Artist;
    }  
    public void setAlbum(String Album){
        this.Album=Album;
    }  
    public String getName(){
        return Name;
    }
    public String getTime(){
        return Time;
    }
    public String getArtist(){
        return Artist;
    }
    public String getAlbum(){
        return Album;
    }
}
