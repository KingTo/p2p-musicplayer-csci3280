
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Reader {
    String filepath;
    data[] header;
    int headerSize;

     class data{
        String name;
        String value;
    }

    public Reader(String path){
        filepath = path;
        readHeaderInfo();
    }
    
    public int getheaderSize(){
        return headerSize;
    }

    void readHeaderInfo(){ 
        FileInputStream audio;
        headerSize = 0;
        final int[] length_of_header = {4, 4, 4, 4, 4, 2, 2, 4, 4, 2, 2, 4, 4};

        try {
            audio = new FileInputStream(filepath);
            header = new data[13];
            for(int i=0;i<13;i++){
                header[i] = new data();
            }
            header[0].name = "ChunkID";
            header[1].name = "ChunkSize";
            header[2].name = "Format";
            header[3].name = "Subchunk1 ID";
            header[4].name = "Subchunk1 Size";
            header[5].name = "Audio Format";
            header[6].name = "Num Channels";
            header[7].name = "Sample Rate";
            header[8].name = "Byte Rate";
            header[9].name = "Block Align";
            header[10].name = "BitsPerSample";
            header[11].name = "Subchunk2 ID";
            header[12].name = "Subchunk2 Size";

            for(int i=0;i<13;i++){
                byte[] buffer = new byte[length_of_header[i]];
                headerSize += length_of_header[i];
                audio.read(buffer);
                if(i==0||i==2||i==3||i==11){    
                    header[i].value = new String(buffer);
                }
                else if(i==1||i==4||i==7||i==8||i==12){ 
                    ByteBuffer temp = ByteBuffer.wrap(buffer);
                    temp.order(ByteOrder.LITTLE_ENDIAN);   
                    header[i].value = Integer.toString(temp.getInt());
                }
                else{   
                    ByteBuffer temp = ByteBuffer.wrap(buffer);
                    temp.order(ByteOrder.LITTLE_ENDIAN);  
                    header[i].value = Short.toString(temp.getShort());
                }
            }
        }catch (IOException e) {
            System.out.println("File is not found!");
        }
    }


}
