package sqlite;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import readSonginfo.Songdata;

public class SqlQuery extends Application{
    private int ResultCount;
    Connection conn = null;
    
    public int getCount(){
        return ResultCount;
    }
    
    
    public void connect() {
        try {
            String url = "jdbc:sqlite:"+System.getProperty("user.dir")+"\\musicinfo.txt";
            conn = DriverManager.getConnection(url);
            
        } catch (SQLException e) {
            try {
                System.out.println(e.getMessage());
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(SqlQuery.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }

    
    public Songdata[] SearchKeyWord(String KeyWord){
        Songdata info[]=new Songdata[100];
        
        int i=0;
        try {
            KeyWord="'%"+KeyWord+"%'";
            String query = "select Name, Time, Artist, Album "+
                    "from musicinfo "+
                    "where Name LIKE "+KeyWord+
                    " OR Time LIKE "+KeyWord+
                    " OR Artist LIKE "+KeyWord+
                    " OR Album LIKE "+KeyWord;
            connect();
            Statement stmt=conn.createStatement();
             ResultSet rs = stmt.executeQuery(query);
            Statement stat = conn.createStatement();
            while (rs.next()){
            String Name =rs.getString("Name");
            String Time =rs.getString("Time");
            String Artist=rs.getString("Artist");
            String Album=rs.getString("Album");
            info[i]=new Songdata(Name,Time,Artist,Album);
            info[i].setName(Name);
            info[i].setTime(Time);
            info[i].setArtist(Artist);
            info[i].setAlbum(Album);
            ResultCount=i;
            i++;
            
            }

        } catch (SQLException ex) {
            Logger.getLogger(SqlQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        return info;
    }
    public void Insert(String name,String time,String artist,String album) throws SQLException{
        connect();
        Statement stmt=conn.createStatement();
        stmt.executeUpdate("INSERT OR IGNORE INTO musicinfo (Name, Time, Artist, Album) VALUES ( '"+name+"', '"+time+"', '"+artist+"', '"+album+"' ) ");
    }
    
    public void Delete(String name,String time,String artist,String album) throws SQLException{
        connect();
        Statement stmt=conn.createStatement();
        stmt.executeUpdate("DELETE FROM musicinfo WHERE Name = '"+name+"' "+"AND Time = '"+time+"' AND Artist = '"+artist+"' AND Album = '"+album+"'");
    }
   
    @Override
    public void start(Stage primaryStage) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

   
}