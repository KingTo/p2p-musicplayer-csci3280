import javafx.fxml.Initializable;
import javafx.event.EventHandler;
import java.awt.event.KeyEvent;
import java.io.IOException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import sqlite.SqlQuery;
import readSonginfo.ReadLyric;
import readSonginfo.Songdata;
import java.util.ResourceBundle;
import java.net.URL;
import static java.lang.Math.log10;
import java.sql.SQLException;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javax.sound.sampled.FloatControl;
public class Controller implements Initializable {
    private Playmusic player;
    private boolean is_playingback = false;
    private boolean playing_music = false;
    public final BooleanProperty syn_is_playingback = new SimpleBooleanProperty(false);
    private long StartTime = System.nanoTime();
    private long EndTime = System.nanoTime();
    private long duetime = EndTime - StartTime;
    private long totaltime=0;
    private long timetemp=0;
    private double lyrictime;
    private double timeinsecond;
    private String[] lyricinfo;
    private Songdata printinfo[];
    private int resultcount;
    private Songdata DataBaseInfo[];
    private SqlQuery dosearch=new SqlQuery();
    private SqlQuery doinsert=new SqlQuery();
    private SqlQuery dodelete=new SqlQuery();
    private SqlQuery find=new SqlQuery();
    private long firstclick;
    private long secondclick;
    private long clicktime=1000000001;
    private boolean firstclickstatus=false;
    private String SongPath=System.getProperty("user.dir")+"\\lyrics\\";
    private String SongPathName;
    private Songdata insertinfo[];
    private String AllLyric="";
    @FXML private Button play;
    @FXML private Slider volumeSlider;
    @FXML private Button removeButton;
    @FXML private TextField filePath;
    @FXML private Button pause;
    @FXML private TextField searchbar;
    @FXML private TextArea lyrics;  
    @FXML private TextField name;
    @FXML private TextField time;
    @FXML private TextField artist;
    @FXML private TextField album;
    @FXML private TextArea All_lyric;
   
    @FXML private TableView<Songdata> result;
    @FXML private TableColumn<Songdata,String> searchName;
    @FXML private TableColumn<Songdata,String> searchTime;
    @FXML private TableColumn<Songdata,String> searchArtist;
    @FXML private TableColumn<Songdata,String> searchAlbum;
    @FXML private Label currentSongLabel;
    @FXML private TextField peer1;
    @FXML private TextField peer2;
    @FXML private TextField peer3;
    @FXML private TextField port1_str;
    @FXML private TextField port2_str;
    @FXML private TextField port3_str;
    @FXML private Button Connect;
    private Client[] clients;
    
    public void showList(){
        ObservableList<Songdata> list = FXCollections.observableArrayList();
        searchName.setCellValueFactory(new PropertyValueFactory("Name"));      
        searchTime.setCellValueFactory(new PropertyValueFactory("Time"));          
        searchArtist.setCellValueFactory(new PropertyValueFactory("Artist"));
        searchAlbum.setCellValueFactory(new PropertyValueFactory("Album"));
        
       for(int i=0;i<=resultcount;i++){  
        list.add(printinfo[i]);        
       }
        result.setItems(list);
       }
 
 
    @Override
    public void initialize(URL location, ResourceBundle resources) {
         clients = new Client[3];
        result.setOnMousePressed(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if(!firstclickstatus){
                firstclick=System.nanoTime();
                firstclickstatus=true;
                }
                else{
                secondclick=System.nanoTime();
                clicktime=secondclick-firstclick;
                firstclick=0;
                firstclickstatus=false;
                }
                if(clicktime/1000000<1000){
                clicktime=1000000001;
                String tempSongPath=SongPath+result.getSelectionModel().getSelectedItem().getName();
                playsong(tempSongPath);
                
                    }
                }
        
        });
        
        printinfo=dosearch.SearchKeyWord("");
        resultcount=dosearch.getCount();
        showList();
        volumeSlider.setValue(90); 
        volumeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)  {
                float volume = (float) volumeSlider.getValue();
                volume /= 100;
                volume *= 2;
                volume = (float) log10(volume) * 20;
                player.changeVolume(volume);
            }
        });
    }

    public void handleKeyReleasedAction(javafx.scene.input.KeyEvent keyEvent) {
        searchbar.textProperty().addListener((observable, oldValue, newValue) ->{

            printinfo=dosearch.SearchKeyWord(searchbar.getText());
            resultcount=dosearch.getCount();

            showList();
        } );

    }

    /* Button Handlers*/
     @FXML protected void handleRemoveButtonAction(ActionEvent event) {
            String deletename=name.getText();
            String deletetime=time.getText();
            String deleteartist=artist.getText();
            String deletealbum=album.getText();
            try {
                dodelete.Delete(deletename, deletetime, deleteartist, deletealbum);
            }catch(SQLException e){
                e.printStackTrace();
            }

            printinfo=dosearch.SearchKeyWord("");
            resultcount=dosearch.getCount();
            showList();

     }

    
    
     @FXML protected void handleSaveButtonAction(ActionEvent event) throws SQLException{

        String insertname;
        String inserttime;
        String insertartist;
        String insertalbum;
         if(name.getText().equals(""))
         insertname="None";
         else
         insertname=name.getText();
         
         if(time.getText().equals(""))
         inserttime="None";
         else
         inserttime=time.getText();
         
         if(artist.getText().equals(""))
         insertartist="None";
         else
         insertartist=artist.getText();
         
         if(album.getText().equals(""))
         insertalbum="None";
         else
         insertalbum=album.getText();
         
         doinsert.Insert(insertname, inserttime, insertartist,insertalbum);
         printinfo=dosearch.SearchKeyWord("");
         resultcount=dosearch.getCount();
         showList();
     
     }

     public void ShowAllLyric(){
         AllLyric="";
        for(int j=0;j<100;j++)
        if(lyricinfo[j]!=null){
        AllLyric+=lyricinfo[j].substring(10,lyricinfo[j].length());
        AllLyric+="\n";
        }
        
        All_lyric.setText(AllLyric);
     }
     
     public void playsong(String songpath){
     if(!is_playingback){
            String path;
            path = songpath+".wav";
            SongPathName=path;
            insertinfo=find.SearchKeyWord(SongPathName.substring(SongPath.length(),SongPathName.length()-4));
            currentSongLabel.setText("Name:   "+insertinfo[0].getName()+"\nTime:    "+insertinfo[0].getTime()+"\nArtist:   "+insertinfo[0].getArtist()+"\nAlbum:  "+insertinfo[0].getAlbum()+"\n");
            String lyricpath=songpath+".txt";
            if(!path.equals("")){
                try{
                    player = new Playmusic(path);
                    player.play();
                    is_playingback = true;
                    playing_music=true;
                    ReadLyric readlyric=new ReadLyric(lyricpath);
                    lyricinfo=readlyric.Readfile();
                    showLyrics();
                    ShowAllLyric();

                }catch(IOException e){
                    filePath.setText("File not Found!");
                    System.out.println(e.getMessage());
                }
            }
            else{
                filePath.setText("Please enter a file path!");
            }

        }
        else{

            player.resume();
            playing_music=true;
        }
     
     }
         
     
    @FXML protected void handlePlayButtonAction(ActionEvent event){
        if(is_playingback)
        playsong(SongPathName);
        else{
        String path=SongPath+filePath.getText();
        playsong(path);
        }
    };

    @FXML protected void handlePauseButtonAction(ActionEvent event)throws IOException {
        if(is_playingback)
            player.pause();
            playing_music=false;

    };

    @FXML protected void handleStopButtonAction(ActionEvent event)throws IOException {
        if(is_playingback){

            player.pause();
            is_playingback = false;
            playing_music=false;
            timetemp=0;
        }

    }
    @FXML protected void handleSearchButtonAction(ActionEvent event)throws IOException {
         printinfo=dosearch.SearchKeyWord(searchbar.getText());
         resultcount=dosearch.getCount();

         showList();
      
    };
        @FXML
    private void handleConnectButtonAction(ActionEvent event) {
        String host1 = peer1.getText();
        String host2 = peer2.getText();
        String host3 = peer3.getText();
        int port1 = 0;
        int port2 = 0;
        int port3 = 0;
        int i;
        if(!(port1_str.getText().equals("")))
            port1 = Integer.parseInt(port1_str.getText());
        if(!(port2_str.getText().equals("")))
            port2 = Integer.parseInt(port2_str.getText());
        if(!(port3_str.getText().equals("")))
            port3 = Integer.parseInt(port3_str.getText());
        
        if(!(host1.equals(""))){
            Client client1 = new Client(host1,port1);
            client1.createSocket();
            clients[0] = client1;
        }
        
        if(!(host2.equals(""))){
            Client client2 = new Client(host2, port2);
            client2.createSocket();
            clients[1] = client2;
        }
        
        //Client client3 = new Client(host3, port3);
        //client3.createSocket();
 
    }

    public void showLyrics(){
        Thread Counttime=new Thread(new Thread(){
            @Override
            public void run(){
                int i=0;
                StartTime = System.nanoTime();
                while(true){
                    if(playing_music==true){
                        EndTime = System.nanoTime();
                        duetime = EndTime - StartTime;
                        totaltime=timetemp+duetime;
                        timeinsecond=totaltime/1000000000;
                    }
                    else{
                      if(!is_playingback)
                            totaltime=0;
                        timetemp=totaltime;
                        StartTime = System.nanoTime();
                    }
                     if(lyricinfo[i]!=null){
            Character minute=lyricinfo[i].charAt(2);
            int minute_int=minute-'0';
            String second=lyricinfo[i].substring(4,8);
            double second_dou=Double.parseDouble(second);
            lyrictime=minute_int*60+second_dou;
        }
        if(timeinsecond>lyrictime&&lyricinfo[i+1]!=null){
        lyrics.setText("\n\n"+lyricinfo[i].substring(10,lyricinfo[i].length())+"\n"+lyricinfo[i+1].substring(10,lyricinfo[i+1].length()));
        i++;
        }  
        else if(timeinsecond>lyrictime&&lyricinfo[i+1]==null&&lyricinfo[i]!=null){
        lyrics.setText("\n\n"+lyricinfo[i].substring(10,lyricinfo[i].length()));
        i++;
        }        


                }


            }
        });
        Counttime.start();


    }


}

