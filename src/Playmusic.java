import javax.sound.sampled.*;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Playmusic {
    private static int Buffersize = 60*1024;
    private SourceDataLine line = null;
    private AudioFormat format = null;
    private String path;
    private Reader info;
    private byte[] audioData;
    private RandomAccessFile rf;
    public final BooleanProperty shouldStop = new SimpleBooleanProperty(false);

    FloatControl volume; 


    public Playmusic(String path)throws IOException{
        this.path = path;
        info = new Reader(path);

        format=new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,Float.valueOf(info.header[7].value),
                Integer.valueOf(info.header[10].value),Integer.valueOf(info.header[6].value),Integer.valueOf(info.header[9].value),
                Float.valueOf(info.header[8].value),false);

        DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class,
                format);
        try{
            line = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            line.open(format);
            volume = (FloatControl) line.getControl(FloatControl.Type.MASTER_GAIN);

        }catch(LineUnavailableException e){
            System.out.println("line is not available.");
        }

        audioData = new byte[Buffersize];
        rf = new RandomAccessFile(path, "r");

    }

    public void play()throws IOException{
        Thread playbackthread = new Thread(new Thread(){

            @Override
            public void run(){
                try {
                    line.start();
                    byte[] audioData = new byte[Buffersize];
                    RandomAccessFile rf = new RandomAccessFile(path, "r");

                    int headersize = info.getheaderSize();
                    rf.seek(headersize);

                    while(true){
                        synchronized (shouldStop){
                            if(shouldStop.get()){
                                try{
                                    shouldStop.wait();
                                } catch (InterruptedException e){
                                    e.printStackTrace();
                                }
                            }
                        }

                        if(rf.read(audioData) !=-1){
                            line.write(audioData,0,Buffersize);
                        } else{
                            break;
                        }
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        });

        playbackthread.start();

    }

    public void pause(){
        synchronized(shouldStop){
            shouldStop.setValue(true);
        }

        line.stop();

    }

    public void resume(){
        synchronized(shouldStop) {
            shouldStop.setValue(false);
            shouldStop.notify();
        }
        line.start();
    }

    public void end()throws IOException{
        line.drain();
        line.close();
        rf.close();
    }

    public void changeVolume(float dB){
        volume.setValue(dB);
    }

}
